#include <ezButton.h>

#include "DHT.h"
#include <EEPROM.h>
#include <MQ135.h>
#include <Arduino.h>
#include <SPI.h>
#include "SSD1306Ascii.h"
#include "SSD1306AsciiSpi.h"

#define PIN_SPI_CS 10
#define PIN_SPI_DC 9
#define PIN_SPI_RESET 8
#define PIN_MQ_ANALOG A2
#define PIN_DHT 6
#define PIN_BUZZER 7
#define PIN_BTN_MENU 3
#define PIN_BTN_UP 4
#define PIN_BTN_DWN 5

#define DEFAULT_SENSOR_RLOAD 10
#define DEFAULT_SENSOR_RZERO 76.63

#define FLIP_SCREEN true

#define MODE_BURNIN 0
#define MODE_MEASURE 1
#define MODE_SETUP 2
#define MODE_SENSOR 3

#define DHTTYPE DHT22   // DHT 22  (AM2302), AM2321

#define STORE_RLOAD 0
#define STORE_RZERO sizeof(float)
#define STORE_BURNIN sizeof(float)*2
#define STORE_BASE_PPM (sizeof(float)*2)+1
#define STORE_ALERT_PPM (sizeof(float)*3)+1

const uint8_t MENU_MAIN  = 0;
const uint8_t MENU_MODE = 1;
const uint8_t MENU_RPARAMS = 2;
const uint8_t MENU_SPARAM = 3;
const uint8_t MENU_OTHER = 4;
const uint8_t MENU_BURNIN_V = 5;
const uint8_t MENU_RESET_V = 6;
const uint8_t MENU_SET_RZERO = 7;
const uint8_t MENU_SET_RLOAD = 8;
const uint8_t MENU_SET_BPPM = 9;
const uint8_t MENU_SET_APPM = 10;

const int UPDATE_TIME_MS =  300;

struct SensorData
{
  float RZero;
  float RZeroC;
  float Resistance;
  float ResistanceC;
  float PPM;
  float PPMC;
  float Humidity;
  float Temp;
};

DHT __dht(PIN_DHT, DHTTYPE);
MQ135 __mq(PIN_MQ_ANALOG);
SSD1306AsciiSpi __oled;
ezButton __upBtn(PIN_BTN_UP);
ezButton __dwnBtn(PIN_BTN_DWN);
ezButton __menuBtn(PIN_BTN_MENU);

int __mode = 0;
unsigned long __burnin_timer = 0;
unsigned long __update_timer = 0;
SensorData __sensorData;
int8_t __currentSelectedOption = 0;
int8_t __currentSetupMenu = MENU_MAIN;
float __basePPM = 200;
float __alertPPM = 700;
byte __inLoop = 0;
byte __clrScr = 0;

void(* resetFunc) (void) = 0;

void getModeName(char *s, uint8_t sz,  uint8_t mode)
{
  switch(mode)
  {
    default:
      strncpy_P(s, (const char *)F("UNKNOWN"), sz);
      break;    
    case MODE_BURNIN:
      strncpy_P(s, (const char *)F("BURNIN"), sz);
      break;
    case MODE_MEASURE:
      strncpy_P(s, (const char *)F("MEASURE"), sz);
      break;    
    case MODE_SETUP:
      strncpy_P(s, (const char *)F("SETUP"), sz);
      break;    
    case MODE_SENSOR:
      strncpy_P(s, (const char *)F("SENSOR"), sz);
      break;    
  }
}

void readSensorData()
{
  __sensorData.Humidity = NAN;
  __sensorData.Temp = NAN;
  __sensorData.RZero = NAN;
  __sensorData.RZeroC = NAN;
  __sensorData.Resistance = NAN;
  __sensorData.ResistanceC = NAN;
  __sensorData.PPM = NAN;
  __sensorData.PPMC = NAN;

  // Reading temperature or humidity takes about 250 milliseconds!
  // Sensor readings may also be up to 2 seconds 'old' (its a very slow sensor)
  float h = __dht.readHumidity();
  // Read temperature as Celsius (the default)
  float t = __dht.readTemperature();
  
  // Check if any reads failed and exit early (to try again).
  if (isnan(h) || isnan(t)) {
    Serial.println(F("Failed to read from DHT sensor!"));
    return;
  }

  __sensorData.Humidity = h;
  __sensorData.Temp = t;
  __sensorData.RZero        = __mq.getRZero();
  __sensorData.RZeroC       = __mq.getCorrectedRZero(__sensorData.Temp, __sensorData.Humidity);
  __sensorData.Resistance   = __mq.getResistance();
  __sensorData.ResistanceC  = __mq.getCorrectedResistance(__sensorData.Temp, __sensorData.Humidity);
  __sensorData.PPM          = __mq.getPPM();
  __sensorData.PPMC         = __mq.getCorrectedPPM(__sensorData.Temp, __sensorData.Humidity); 
  Serial.print(F("Sensor Data: T["));
  Serial.print(__sensorData.Temp);
  Serial.print(F("] H ["));
  Serial.print(__sensorData.Humidity);
  Serial.print(F("] RZ ["));
  Serial.print(__sensorData.RZero);
  Serial.print(F("] RZC ["));
  Serial.print(__sensorData.RZeroC);
  Serial.print(F("] R ["));
  Serial.print(__sensorData.Resistance);
  Serial.print(F("] RC ["));
  Serial.print(__sensorData.ResistanceC);
  Serial.print(F("] PPM ["));
  Serial.print(__sensorData.PPM);
  Serial.print(F("] PPMC ["));
  Serial.print(__sensorData.PPMC);
  Serial.println(F("]"));


}

void onDwn()
{
  __currentSelectedOption += 1;
}

void onUp()
{
  __currentSelectedOption -= 1;
}

void onClick()
{
  if( __mode == MODE_BURNIN )
  {
    return; // ignore clicks  during burnin process
  }

  if(__mode != MODE_SETUP)
  {
    __mode = MODE_SETUP;
    __oled.clear();    
  }
  else
  {
    Serial.print(F("Current Menu: ")); Serial.print(__currentSetupMenu); Serial.print(F(" Current Option: ")); Serial.print(__currentSelectedOption); Serial.println();
    // process menu selectin here
    if(__currentSetupMenu == MENU_MAIN )
    {
      switch(__currentSelectedOption)
      {
        case 0:
          __currentSetupMenu = MENU_MODE;
          __currentSelectedOption = 0;
          __oled.clear();
          break;
        case 1:
          __currentSetupMenu = MENU_RPARAMS;
          __currentSelectedOption = 0;
          __oled.clear();
          break;
        case 2:
          __currentSetupMenu = MENU_SPARAM;
          __currentSelectedOption = 0;
          __oled.clear();
          break;            
        case 3:
          __currentSetupMenu = MENU_OTHER;
          __currentSelectedOption = 0;
          __oled.clear();
          break;            
      }
    } 
    else if(__currentSetupMenu == MENU_MODE)
    {
      switch(__currentSelectedOption)
      {
        case 0: // sensor mode
          __mode = MODE_SENSOR;
          __currentSetupMenu = MENU_MAIN;
          __currentSelectedOption = 0;          
          __oled.clear();
          break;
        case 1: // measure mode
          __mode = MODE_MEASURE;
          __currentSetupMenu = MENU_MAIN;
          __currentSelectedOption = 0;
          __oled.clear();
          break;    
        case 2:
          __currentSetupMenu = MENU_MAIN;
          __currentSelectedOption = 0;
          __oled.clear();        
          break;
      }
    }
    else if(__currentSetupMenu == MENU_RPARAMS)
    {
      switch(__currentSelectedOption)
      {
        case 0: 
          __currentSetupMenu = MENU_SET_RZERO;
          __currentSelectedOption = 0;
          __oled.clear();
          break;
        case 1: // measure mode
          __currentSetupMenu = MENU_SET_RLOAD;
          __currentSelectedOption = 0;
          __oled.clear();
          break;      
        case 2:
          __currentSetupMenu = MENU_MAIN;
          __currentSelectedOption = 0;
          __oled.clear();        
          break;                    
      }
    }
    else if(__currentSetupMenu == MENU_SPARAM)
    {
      switch(__currentSelectedOption)
      {
        case 0: 
          __currentSetupMenu = MENU_SET_BPPM;
          __currentSelectedOption = 0;
          __oled.clear();
          break;
        case 1: // measure mode
          __currentSetupMenu = MENU_SET_APPM;
          __currentSelectedOption = 0;
          __oled.clear();
          break;      
        case 2:
          __currentSetupMenu = MENU_MAIN;
          __currentSelectedOption = 0;
          __oled.clear();        
          break;                    
      }
    }
    else if(__currentSetupMenu == MENU_OTHER)
    {
      switch(__currentSelectedOption)
      {
        case 0: // sensor mode
          __currentSetupMenu = MENU_BURNIN_V;
          __currentSelectedOption = 0;
          __oled.clear();
          break;
        case 1: // measure mode
          __currentSetupMenu = MENU_RESET_V;
          __currentSelectedOption = 0;
          __oled.clear();
          break;   
        case 2:
          __currentSetupMenu = MENU_MAIN;
          __currentSelectedOption = 0;
          __oled.clear();        
          break;                       
      }
    }
    else if(__currentSetupMenu == MENU_BURNIN_V)
    {
      switch(__currentSelectedOption)
      {
        case 0: 
          EEPROM.put(STORE_BURNIN, byte(0));
          resetFunc();
          break;
        case 1: // measure mode
          __currentSetupMenu = MENU_MAIN;
          __currentSelectedOption = 0;
          __oled.clear();              
          break;            
      }        
    }
    else if(__currentSetupMenu == MENU_RESET_V)
    {
      switch(__currentSelectedOption)
      {
        case 0: 
          resetFunc();
          break;
        case 1: 
          __currentSetupMenu = MENU_MAIN;
          __currentSelectedOption = 0;
          __oled.clear();              
          break;            
      }        
    }
    else if(__currentSetupMenu == MENU_SET_RZERO)
    {
      if(__currentSelectedOption == 1)
      {
          __currentSetupMenu = MENU_RPARAMS;
          __currentSelectedOption = 0;
          __oled.clear();        
      }
      else
      {
        readSensorData();
        float rzero = __sensorData.RZero;
        if(isnan(rzero))
        {
          Serial.println(F("NaN R0! Setting Default instead."));
          rzero = DEFAULT_SENSOR_RZERO;
        }

        EEPROM.put(STORE_RZERO, rzero);
        resetFunc();
      }
    }
    else if(__currentSetupMenu == MENU_SET_RLOAD)
    {
      if(__currentSelectedOption == 4)
      {
        __currentSetupMenu = MENU_RPARAMS;
        __currentSelectedOption = 0;
        __oled.clear();                 
      }
      else
      {
        float rload = DEFAULT_SENSOR_RLOAD;
        if( __currentSelectedOption == 0 )
          rload = 10;
        else if(__currentSelectedOption == 1)
          rload = 22;
        else if(__currentSelectedOption == 2)
          rload = 100;
        else if(__currentSelectedOption == 3)
          rload = 200;

        EEPROM.put(STORE_RLOAD, rload);
        resetFunc();             
      }
    }
    else if(__currentSetupMenu == MENU_SET_BPPM)
    {
      switch(__currentSelectedOption)
      {
        case 0: // sensor mode
          __basePPM = min(__basePPM + 20, 5000);
          break;
        case 1: // measure mode
          __basePPM = max(__basePPM - 20, 000);
          break;   
        case 2:
          EEPROM.put(STORE_BASE_PPM, __basePPM);
          __currentSetupMenu = MENU_MAIN;
          __currentSelectedOption = 0;
          __oled.clear();
          break;                      
        case 3:
          __currentSetupMenu = MENU_SPARAM;
          __currentSelectedOption = 0;
          __oled.clear();
          break; 
      }      
    }   
    else if(__currentSetupMenu == MENU_SET_APPM)
    {
      switch(__currentSelectedOption)
      {
        case 0: // sensor mode
          __alertPPM = min(__alertPPM + 20, 5000);
          break;
        case 1: // measure mode
          __alertPPM = max(__alertPPM - 20, 000);
          break;   
        case 2:
          EEPROM.put(STORE_ALERT_PPM, __alertPPM);
          __currentSetupMenu = MENU_MAIN;
          __currentSelectedOption = 0;
          __oled.clear();
          break;                      
        case 3:
          __currentSetupMenu = MENU_SPARAM;
          __currentSelectedOption = 0;
          __oled.clear();
          break; 
      }      
    }        
  }
}

void isr_menu()
{
  if(!__inLoop)
    return;

  if( __mode != MODE_SETUP && __mode != MODE_BURNIN)
  {
    __mode = MODE_SETUP;
    __clrScr = 1;
  }
}

void setup() {
  Serial.begin(9600);
  Serial.println(F("---- SETUP ----"));

  memset(&__sensorData, 0, sizeof(SensorData));

  __oled.begin(&SH1106_128x64, PIN_SPI_CS, PIN_SPI_DC, PIN_SPI_RESET);  
  __oled.setFont(System5x7);
  __oled.displayRemap(FLIP_SCREEN);
  __oled.clear();
  __oled.println(F("AirSensor v1.0"));
  __oled.println(F("[Author: r4z]"));
  __oled.println(F("Initializing..."));

  // load rload and rzero from eeprom or default
  Serial.println(F("Reading EEPROM"));
  float sensor_rload = 0;
  float sensor_rzero = 0;
  byte burned_in = 0;
  
  EEPROM.get(STORE_RLOAD, sensor_rload);
  Serial.print(F("EEPROM [RLOAD]: ")); Serial.println(sensor_rload);
  EEPROM.get(STORE_RZERO, sensor_rzero);  
  Serial.print(F("EEPROM [RZERO]: ")); Serial.println(sensor_rzero);
  EEPROM.get(STORE_BURNIN, burned_in);
  Serial.print(F("EEPROM [BURNIN]: ")); Serial.println(burned_in);
  EEPROM.get(STORE_BASE_PPM, __basePPM);
  Serial.print(F("EEPROM [BASE_PPM]: ")); Serial.println(__basePPM);
  EEPROM.get(STORE_ALERT_PPM, __alertPPM);
  Serial.print(F("EEPROM [ALERT_PPM]: ")); Serial.println(__alertPPM);
  

  
  sensor_rload = (sensor_rload <= 0) ? DEFAULT_SENSOR_RLOAD : sensor_rload;
  sensor_rzero = (sensor_rzero <= 0) ? DEFAULT_SENSOR_RZERO : sensor_rzero;
  Serial.println(F("Creating MQ Sensor"));
  __mq = MQ135(PIN_MQ_ANALOG, sensor_rzero, sensor_rload);
  Serial.println(F("Starting DHT Sensor"));
  __dht.begin();
    
  if( burned_in == 0 )
  {
    __mode = MODE_BURNIN;    
  }
  else
  {
    __mode = MODE_SENSOR;
  }

  // setup buzzer:
  pinMode(PIN_BUZZER, OUTPUT);
  digitalWrite(PIN_BUZZER, LOW);

  // buttons
  __upBtn.setDebounceTime(50);
  __dwnBtn.setDebounceTime(50);
  __menuBtn.setDebounceTime(50);
  __upBtn.setCountMode(COUNT_RISING);
  __dwnBtn.setCountMode(COUNT_RISING);
  __menuBtn.setCountMode(COUNT_RISING);
  attachInterrupt(digitalPinToInterrupt(PIN_BTN_MENU), isr_menu, RISING);

  
  delay(5000);
  __oled.clear();
}

void loop() 
{
  unsigned long timeMS = millis();  
  if(timeMS < 120000ul && (__mode != MODE_BURNIN))
  { 
    // if less than 120sec wait for sensor to heat up...
    __oled.home();
    __oled.println();
    __oled.println(F("Sensor Heating Up..."));
    __oled.print(F("Progress: "));
    __oled.print((timeMS*100)/120000ul);
    __oled.println(F("%")); 
    delay(300);
    return;   
  }

  __inLoop = 1;
  __upBtn.loop();
  __dwnBtn.loop();
  __menuBtn.loop();

  if( __upBtn.getCount() > 0)
    onUp();
  
  if( __dwnBtn.getCount() > 0)
    onDwn();
          
  if( __menuBtn.getCount() > 0)
    onClick();

  char modeStr[8];
  getModeName(modeStr, 8, __mode);  
  
  if(__clrScr == 0){
    __oled.home();
  } else {
    __oled.clear();
    __clrScr = 0;
    digitalWrite(PIN_BUZZER, LOW);    
  }

  __oled.print(F("Mode: ")); __oled.println(modeStr);
  
  if(__mode != MODE_BURNIN && __mode != MODE_SETUP)
  {
    // Wait a few seconds between measurements.
    delay(UPDATE_TIME_MS);
    readSensorData();

    if(__mode == MODE_MEASURE)
    {
      __oled.print(F("T: ")); __oled.print(__sensorData.Temp); __oled.print(F(" RH: ")); __oled.println(__sensorData.Humidity);
      __oled.print(F("RO: ")); __oled.print(__sensorData.RZero); __oled.print(F(" ROc: ")); __oled.println(__sensorData.RZeroC);
      __oled.print(F("R: ")); __oled.print(__sensorData.Resistance); __oled.print(F(" Rc: ")); __oled.println(__sensorData.ResistanceC);
      __oled.print(F("P: ")); __oled.print(__sensorData.PPM); __oled.print(F(" Pc: ")); __oled.println(__sensorData.PPMC);      
    }
    else
    {
      __oled.print(F("T: ")); __oled.print(__sensorData.Temp); __oled.print(F(" RH: ")); __oled.println(__sensorData.Humidity);
      __oled.print(F("RO: ")); __oled.print(__sensorData.RZero); __oled.print(F(" ROc: ")); __oled.println(__sensorData.RZeroC);
      __oled.print(F("R: ")); __oled.print(__sensorData.Resistance); __oled.print(F(" Rc: ")); __oled.println(__sensorData.ResistanceC);
      __oled.print(F("P: ")); __oled.print(__sensorData.PPM); __oled.print(F(" Pc: ")); __oled.println(__sensorData.PPMC);
      __oled.print(F("pB: ")); __oled.print(__basePPM); __oled.print(F(" pA: ")); __oled.println(__alertPPM);

      float t = ((__basePPM * (__alertPPM / 100)) + __basePPM);
      if( isnan(__sensorData.PPM))
      {
        __oled.println(F("PPM is NaN!!!"));
        digitalWrite(PIN_BUZZER, HIGH);
      }
      else if(__sensorData.PPM <  t)
      {
        __oled.println(F("All is Good!"));        
      }
      else
      {
        __oled.println(F("High PPM Detected!"));
        digitalWrite(PIN_BUZZER, HIGH);        
      }
    }
  }
  else
  {
    if(__mode == MODE_BURNIN)
    {
      // Burnin mode - dont use sensor for measurements until completed.
      float pct = ((float)timeMS / 86400000)*100;    
      pct = (pct > 100.0) ? 100.0 : pct;
      
      __oled.println();
      __oled.println(F("BurnIn in progress!"));
      __oled.println();
      __oled.println(F("...Keep Powered..."));
      __oled.println();
      __oled.print(F("Progress: "));
      __oled.print(pct);
      __oled.println(F("%"));    

      if(pct >= 100.0)
      {
        __oled.clear();
        __oled.println(F("BurnIn Complete!"));
        __oled.println(F("Resetting Device..."));
        delay(5000);

        // { // TODO: this is for testing, replace with EEPROM write and reset call
        //   __oled.clear();
        //   __mode = MODE_MEASURE; 
        // }
        EEPROM.put(sizeof(float)*2, byte(1));
        resetFunc();
      }
    }
    else
    { // SETUP
      __oled.println();
      switch( __currentSetupMenu )
      {
        case MENU_MAIN:
          __currentSelectedOption = __currentSelectedOption > 3 ? 0 : __currentSelectedOption < 0 ? 3 : __currentSelectedOption;
          __oled.setInvertMode(__currentSelectedOption == 0);
          __oled.println(F("Set Mode"));
          __oled.setInvertMode(__currentSelectedOption == 1);
          __oled.println(F("Set R Params"));
          __oled.setInvertMode(__currentSelectedOption == 2);
          __oled.println(F("Set Sensor Params"));
          __oled.setInvertMode(__currentSelectedOption == 3);
          __oled.println(F("Other"));          
          __oled.setInvertMode(false);
        break;        

        case MENU_MODE:
          __currentSelectedOption = __currentSelectedOption > 2 ? 0 : __currentSelectedOption < 0 ? 2 : __currentSelectedOption;
          __oled.setInvertMode(__currentSelectedOption == 0);
          __oled.println(F("Sensor Mode"));
          __oled.setInvertMode(__currentSelectedOption == 1);
          __oled.println(F("Measure Mode"));
          __oled.setInvertMode(__currentSelectedOption == 2);
          __oled.println(F("Back"));          
          __oled.setInvertMode(false);
        break;

        case MENU_RPARAMS:
          __currentSelectedOption = __currentSelectedOption > 2 ? 0 : __currentSelectedOption < 0 ? 2 : __currentSelectedOption;
          __oled.setInvertMode(__currentSelectedOption == 0);
          __oled.println(F("Set RZero"));
          __oled.setInvertMode(__currentSelectedOption == 1);
          __oled.println(F("Set RLoad"));   
          __oled.setInvertMode(__currentSelectedOption == 2);
          __oled.println(F("Back"));                    
          __oled.setInvertMode(false);
        break;

        case MENU_SPARAM:
          __currentSelectedOption = __currentSelectedOption > 2 ? 0 : __currentSelectedOption < 0 ? 2 : __currentSelectedOption;
          __oled.setInvertMode(__currentSelectedOption == 0);
          __oled.println(F("Set Base PPM"));
          __oled.setInvertMode(__currentSelectedOption == 1);
          __oled.println(F("Set Alarm PPM"));      
          __oled.setInvertMode(__currentSelectedOption == 2);
          __oled.println(F("Back"));                    
          __oled.setInvertMode(false);
        break;

        case MENU_OTHER:
          __currentSelectedOption = __currentSelectedOption > 2 ? 0 : __currentSelectedOption < 0 ? 2 : __currentSelectedOption;          
          __oled.setInvertMode(__currentSelectedOption == 0);
          __oled.println(F("Force BurnIn"));
          __oled.setInvertMode(__currentSelectedOption == 1);
          __oled.println(F("Reset Device"));        
          __oled.setInvertMode(__currentSelectedOption == 2);
          __oled.println(F("Back"));                    
          __oled.setInvertMode(false);
        break;

        case MENU_RESET_V:
        case MENU_BURNIN_V:
          __currentSelectedOption = __currentSelectedOption > 1 ? 0 : __currentSelectedOption < 0 ? 1 : __currentSelectedOption;          
          __oled.println(F("Are You Sure?"));
          __oled.setInvertMode(__currentSelectedOption == 0);
          __oled.println(F("Im Sure!"));
          __oled.setInvertMode(__currentSelectedOption == 1);
          __oled.println(F("Nevermind..."));        
          __oled.setInvertMode(false);          
        break;
          
        case MENU_SET_RZERO:
          __currentSelectedOption = __currentSelectedOption > 1 ? 0 : __currentSelectedOption < 0 ? 1 : __currentSelectedOption;          
          __oled.println(F("Will set from sensor"));
          __oled.setInvertMode(__currentSelectedOption == 0);
          __oled.println(F("Set"));
          __oled.setInvertMode(__currentSelectedOption == 1);
          __oled.println(F("Back"));                    
          __oled.setInvertMode(false);
        break;
        
        case MENU_SET_RLOAD:
          __currentSelectedOption = __currentSelectedOption > 4 ? 0 : __currentSelectedOption < 0 ? 4 : __currentSelectedOption;
          __oled.setInvertMode(__currentSelectedOption == 0);
          __oled.println(F("10K"));
          __oled.setInvertMode(__currentSelectedOption == 1);
          __oled.println(F("22K"));
          __oled.setInvertMode(__currentSelectedOption == 2);
          __oled.println(F("100K"));
          __oled.setInvertMode(__currentSelectedOption == 3);
          __oled.println(F("200K"));
          __oled.setInvertMode(__currentSelectedOption == 4);
          __oled.println(F("Back"));           
          __oled.setInvertMode(false);
        break;

        case MENU_SET_BPPM:
          __currentSelectedOption = __currentSelectedOption > 3 ? 0 : __currentSelectedOption < 0 ? 3 : __currentSelectedOption;
          __oled.print(F("PPM Value: "));
          __oled.println(__basePPM); 
          __oled.setInvertMode(__currentSelectedOption == 0);
          __oled.println(F("Decrease"));
          __oled.setInvertMode(__currentSelectedOption == 1);
          __oled.println(F("Increase"));
          __oled.setInvertMode(__currentSelectedOption == 2);
          __oled.println(F("Save"));           
          __oled.setInvertMode(__currentSelectedOption == 3);
          __oled.println(F("Back"));          
          __oled.setInvertMode(false);         
        break;

        case MENU_SET_APPM:
          __currentSelectedOption = __currentSelectedOption > 3 ? 0 : __currentSelectedOption < 0 ? 3 : __currentSelectedOption;
          __oled.print(F("PPM Value: "));
          __oled.println(__alertPPM); 
          __oled.setInvertMode(__currentSelectedOption == 0);
          __oled.println(F("Decrease"));
          __oled.setInvertMode(__currentSelectedOption == 1);
          __oled.println(F("Increase"));
          __oled.setInvertMode(__currentSelectedOption == 2);
          __oled.println(F("Save"));           
          __oled.setInvertMode(__currentSelectedOption == 3);
          __oled.println(F("Back"));          
          __oled.setInvertMode(false);         
        break;        
        
        default:
        break;
      }
    }
  }

  __upBtn.resetCount();
  __dwnBtn.resetCount();
  __menuBtn.resetCount();
}

// Sensor Mode
// Measure Mode
// Set RZero
// Set RLoad
// Set Base PPM
// Set Alarm
// Force Burnin
// Reset Device
