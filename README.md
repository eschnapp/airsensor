# AirSensor

## General
This project involved creating a simple air quality sensor for my home made fume hood  using an MQ135 Sensor and an Arduino Nano as driver. The purpose of this project was to setup a simple gas detector to alert if any gases such as NH4 or NOx escape the fume hood during operation and alert using a 5v buzzer.

In addition, I have added the ability to re-calibrate the sensor based on clean air conditions and a burn-in process required for initial usage.

## Hardware

The following hardware was used in the project:
- [MQ135 Sensor package](https://www.amazon.com/dp/B07L73VTTY?psc=1&ref=ppx_yo2ov_dt_b_product_details)
- [DHT22 (AM2302) Temp/Humidity Sensor](https://www.amazon.com/dp/B08XTC8CQX?psc=1&ref=ppx_yo2ov_dt_b_product_details)
- Arduino Nano [(I Used MS cheap brand)](https://www.microcenter.com/product/615097/inland-nano-development-board-arduino-compatible) - If you use cheaper brands I recommend using a USB hub or extension module to prevent burning off your PC USB on-board driver (from experience).
- [Arduino Nano Shield](https://www.microcenter.com/product/632701/inland-nano-shield-board-w-power-switch)
- [128x65 OLED SPI Display](https://www.microcenter.com/product/643965/inland-iic-spi-13-128x64-oled-v20-graphic-display-module-for-arduino-uno-r3)
- [5v DC Buzzer](https://www.microcenter.com/product/662094/philmore-b814-3v-dc-miniature-buzzer)
- [3 Tactile momentary switches](https://www.microcenter.com/product/632688/inland-6x6mm-micro-momentary-tactile-push-button-switches-assortment-kit-10-values-180-pcs)

### Some HW Related Notes
- The MQ-135 requires an initial 24h BurnIn process (described below) and a Calibration process to find the correct R0 value (also described below). The software accomodates for those and allows to re-calibrate as needed.
- all current connceted pins (based on diagram) are configurable in the code and can be changes as needed, with the following considerations:
  -  oled module uses hardware SPI pins (D13/SCK and D11/MOSI on Nano) which are set. CS and DC pins can be any digital pin
  -  Menu/OK Button must be plugged into Hardware Interrupt pin (D2/D3) in order to wake up from Sensor/Measure mode.
  -  Sensor A0 pin should be connected to any Analog pin on Nano (D0 pin is not used as of now)


## Setup
### Circuit Diagram
Below is a Fritzing BB and Schema diagrams of the circuit (excluding power supply)
![Breadboard](air_sensor_bb.png)
![Schema](air_sensor_schem.png)
![Test](Pics/20230318_233514.jpg)

### MQ-135 Spcifics
Thre are a few things about the MQ-135 that needs to be considered (mor information is available in the reference section below):

- The sensor needs to undergo a Burn-In process before it can be accurately used. This involves powering up the circuit and leaving it on for 24 hours to allow the SnO2 fillament to heat up. The software will require a Burn-In to take place on initial use (and can be forced to re burn if a new sensor is installed).
- As described in the referenced below, the Technical sheet calls for a load resistor (RL) of ~20KΩ while many cheap versions come with a 1KΩ resistor. Its highly recommended to replace that resistor with a 22KΩ resistor (or at least 10KΩ) for more accurate reading.
- When initially powered, the Sensor needs approximately 120 Seconds to come up to measuring temperature (Internal fillament heats up).
- Before accurate measurements can be taken, the sensor R0 value needs to be calibrated by taking the sensor outside, waiting for 120 seconds and then taking multiple readings of resistance value over 2-5 minutes, averaging the results and setting the RZero value in software to the resulted value. 


## Enclosure
I decided to design my enclosure with separate main box for the board, screen buttons and buzzer and a separate smaller enclosure for the MQ135 and DHT sensors, which is wired via a USB cable to the main box. This allowes me to mount the main box on the wall while placing the sensor itself in the designated measuring area (in my case right by the Fumehood's vent connection).

The bride parts of the enclosure are designed to be printed separately and then glued into the main box.

![Sample1](Pics/20230325_173733.jpg)
![Sample2](Pics/20230325_175614.jpg)
![Sample3](Pics/20230325_180153.jpg)
![Sample4](Pics/20230325_181100.jpg)
![Sample5](Pics/20230325_181215.jpg)
![Sample6](Pics/20230325_181221.jpg)
![Sample7](Pics/20230325_181239.jpg)

# Software

The Software is designed to be self contained, and should not need re-upload unless hardware is modified (i.e. new hardware added or pin configuration changes). 

## Startup
When initially powered, the Nano will go through its startup sequence, during which a prompt is displayed. This usually takes approx ~5 seconds to aqquire all hardware state and initial sensor data.

When the Startup process is complete, if BurnIn is not required - a 120 second WarmUp process will commence after which the Software will move automatically into Sensor mode.

## Operation
During operation the software can be in 1 out of 4 modes:

### Burn-In
This mode is initiated on first startup, and after a Forced BurnIn is selected. It will prompt the user to keep the device powered for 24 hours and display a progress bar.
If the device is unpowered during BurnIn the process will restart itself on next power up. 

There is no action possible during the BurnIn process.

###  Measure
This mode continously measures Sensor data from the MQ-135 and the DHT sensors and displays it on the screen. Can be used for general measurements and for R0 calibration process.

### Sensor
This mode is similar to Measure mode with the following additions:
- Alarm will be sound if current measured PPM is NaN (possible hardware issue)
- Alarm will be sound if measured PPM is higher than AlertPPM percentage over BasePPM value. The intent here is to set a BasePPM which is normal PPM value at the location of the sensor and alarm will sound if that value goes over a certain % (AlertPPM)
- If all measurements are within acceptable range, All Good message is displayed.

### Setup
This mode allows for general setup and configurations:
- Mode select: allows to pick Sensor or Measure mode
- Set R Params: allows to set the resistanc params (R0 and RL) into EEPROM memory (will force-reset after save)
- Set S Params: allows to set the sensor params BasePPM and AlertPPM%
- Other: allows to either force a Burn-In process or Reset the device

# Other
## References
Below is a short list of references for materials I used while working on this project
- [MQ-135 Datasheet olimex](https://www.olimex.com/Products/Components/Sensors/Gas/SNS-MQ135/resources/SNS-MQ135.pdf)
- [Setup Instructions](https://www.olimex.com/Products/Components/Sensors/_resources/SNS-MQ-sensors.pdf)
- [Another Datasheet Hanwei](https://datasheet-pdf.com/PDF/MQ135-Datasheet-Hanwei-605077)
- [How to Use MQ-135 Gas Sensor](https://www.codrey.com/electronic-circuits/how-to-use-mq-135-gas-sensor/)
- [Calibration procedure for a MQ135](https://forum.allaboutcircuits.com/threads/calibration-procedure-of-a-mq135.189025/)
- [Cheap CO2 meter  using MQ135 by Davide Gironi](http://davidegironi.blogspot.com/2014/01/cheap-co2-meter-using-mq135-sensor-with.html#.VJXAgsC3b)
- [MQ135 Arduino Library](https://hackaday.io/project/3475-sniffing-trinket/log/12363-mq135-arduino-library)


## Future Improvements

Some future changes and enhancements I plan to work on in the future:
- Ability to pick default mode on startup (sensor/measure)
- Ability calculate specific gas PPMs
- Add Wifi module and web console